#!/usr/bin/env python
import datetime
import sys
import operator
import functools

HELP = f"""\
Usage: {sys.argv[0]} [durations...]

where [duration] is of the form "[hours]:[minutes]" or "[minutes]".

Sum the durations given as command-line arguments of the form
"[hours]:[minutes]" or "[minutes]"; e.g. "5:20" for five hours and 20 minutes
or "45" for 45 minutes.  The result is printed to standard output in the form
[hours]:[minutes]:[sections].

Example:
    $ {sys.argv[0]} 5:20 45
    6:05:00
"""

sum_deltas = lambda iter: functools.reduce(operator.add, iter,
                                           datetime.timedelta(seconds=0))

def to_delta(s):
    if ':' in s:
        minutes, hours = map(int, reversed(s.split(':')))
    else:
        minutes = int(s)
        hours = 0
    return datetime.timedelta(hours=hours, minutes=minutes)

print(sum_deltas(map(to_delta, sys.argv[1:])))
