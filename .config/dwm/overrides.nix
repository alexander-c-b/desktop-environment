oldAttrs: rec {
  patches = let
    fibonacci = builtins.fetchurl {
      url    = "http://dwm.suckless.org/patches/fibonacci/dwm-fibonacci-6.2.diff";
      sha256 = "12y4kknly5irwd6yhqj1zfr3h06hixi2p7ybjymhhhy0ixr7c49d";
    };
    fancybar = builtins.fetchurl {
      url    = "http://dwm.suckless.org/patches/fancybar/dwm-fancybar-6.2.diff";
      sha256 = "0bf55553p848g82jrmdahnavm9al6fzmd2xi1dgacxlwbw8j1xpz";
    };
    noborder = builtins.fetchurl {
      url    = "http://dwm.suckless.org/patches/noborder/dwm-noborderfloatingfix-6.2.diff";
      sha256 = "114xcy1qipq6cyyc051yy27aqqkfrhrv9gjn8fli6gmkr0x6pk52";
    };
    push     = builtins.fetchurl {
      url    = "https://dwm.suckless.org/patches/push/dwm-push-20201112-61bb8b2.diff";
      sha256 = "19ln6q29ds2ifm9i7s0cvvadn7i4s2m65w9zq22gc4w3rsqfqv3w";
    };
  in oldAttrs.patches ++ [ fibonacci fancybar noborder push ];

  conf = ./config.h;
  # based on pkgs.dwm-git
  postPatch = ''
    cat '${conf}' > config.def.h
  '';
}
