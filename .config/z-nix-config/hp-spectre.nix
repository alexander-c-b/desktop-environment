{ config, pkgs, ... }:

{
  # Boot loader
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint     = "/boot";
    };
    systemd-boot.enable = false;
    grub = {
      enable     = true;
      efiSupport = true;
      device     = "nodev";
      default    = 1;
      extraEntries = ''
        menuentry "Windows 10" {
          chainloader /EFI/Microsoft/Boot/bootmgfw.efi
        }
      '';
    };
  };


  # Networking
  networking = {
    networkmanager.enable      = true;
    useDHCP                    = false;
    interfaces.wlp1s0.useDHCP  = true;
    hostName                   = "z-nixos-spectre-laptop";
    hostId                     = "81b1ea5b";
  };

  services.xserver = {
    # Enable touchpad support.
    libinput.enable = true;
    libinput.touchpad = {
      tapping          = true;
      naturalScrolling = true;
      scrollMethod     = "twofinger";
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

