pkgs:

let

  package = name: pkgs.callPackage (./packages + "/${name}.nix") {};

  st = import (pkgs.fetchFromGitHub {
    owner  = "alexander-c-b";
    repo   = "st";
    rev    = "8221ef83dcd760ff45a9cdebda539f2749852478";
    sha256 = "0jk33qzf3vihdnwsaf935fxjqjmhp19xginv3gibcih0r8ffcnnx";
  }) { inherit pkgs; };

  myGhc = pkgs.haskellPackages.ghcWithPackages (packages: with packages; [
    containers
    text
    split
    pandoc-types
    pandoc
  ]);

in rec {
  # Barebones system packages, available in all user environments
  system = with pkgs; [
    vim
    light
    (package "nvidia-offload")
  ];

  # My most basic terminal packages
  basic = with pkgs; [
    zsh
    neovim
    luajit
    direnv
    nix-direnv
    file
    tree
    git
    git-lfs
    glab
    gh
    wget
    htop
    dash
    killall
    trash-cli
    pdftk
    unzip
    parallel
    nixfmt
    (package "vimv")

    # Programming environments
    (python38.withPackages (ps: with ps; [
      ipython sympy
    ]))
  ];

  # Media/Text Processing
  mediaProcessing = with pkgs; [
    texlive.combined.scheme-full
    ffmpeg
    imagemagickBig
    myGhc
  ];

  # My personal graphical packages
  graphical = with pkgs; [
    # Interface
    dmenu
    st
    setroot # for background

    # X.Org utilities
    xorg.xmodmap
    xbindkeys
    xdotool
    xclip

    # Daily Tools
    qutebrowser
    zathura

    # Terminal Tools
    maim
    feh

    # Other Utilities
    firefox
    google-chrome
    gnumeric
    anki
    vlc
    gimp
    zoom-us
    teams

    # Miscellaneous
    simple-scan

    # Audio
    clementine
    audacity

    # Internet
    networkmanagerapplet

    # Password Management
    pass
    xkcdpass
    gnupg
    pinentry-curses
    pinentry-gtk2
  ];
}
