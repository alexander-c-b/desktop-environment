# vim: set foldmethod=marker:
##### Basics {{{1
source $HOME/.profile

# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}

##### Initialize ZIMFW {{{1

if [[ ${ZIM_HOME}/init.zsh -ot ${ZIM_HOME}/.zimrc ]]; then
  # Update static initialization script if it's outdated, before sourcing it
  source ${ZIM_HOME}/zimfw.zsh init -q
fi
source ${ZIM_HOME}/init.zsh

# Set what highlighters will be used.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

##### HISTFILE {{{1
HISTFILE=~/.histfile
HISTSIZE=1500000000
SAVEHIST=1000000000
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS

##### Other Setup {{{1
# direnv
if which direnv 1> /dev/null
then
    eval "$(direnv hook zsh)"
fi

# Completion for gh and glab
fpath=($HOME/.local/share/zsh/site-functions
       $HOME/.config/zsh/zsh-completions $fpath)
if [ ! -e $HOME/.local/share/zsh/site-functions ]; then
    local location=$HOME/.local/share/zsh/site-functions
    mkdir -p $location
    glab completion -s zsh > $location/_glab
    gh   completion -s zsh > $location/_gh
fi

fpath=($HOME/.config/zsh/zsh-manydots-magic/ $fpath)
autoload -Uz manydots-magic
manydots-magic

source $HOME/.config/zsh/zsh-nix-shell/nix-shell.plugin.zsh

source `which env_parallel.zsh`

autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd ' ' edit-command-line

export GHCHOME="$HOME/.ghc"
export LESS=FXR

##### compinstall {{{1
zstyle ':completion:*' completer _complete _ignored
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit

##### Functions & Aliases {{{1
function nixr {
    nixr_1="$1"
    shift
    nix run -f '<nixos>' $nixr_1 -c $(sed -E 's/.*?\.//' <<< $nixr_1) $@
}

alias nixi="nix-env -f '<nixos>' -iA"

alias feh="feh --keep-zoom-vp"

function count { printf $'%s\n' $#; }
alias gis="git status"
alias gia="git add"
alias gid="git diff"
alias gic="git checkout"
alias gicb="git checkout -b"
alias gilog="git log --oneline -n3"
function gicm  { message="$@"; git commit -m "$message"; }
function cdgit { cd $(git rev-parse --show-toplevel); }
function into { mkdir -p "$@" && cd "$1"; }

alias ls='ls --color=auto'
alias diff='diff --color=auto'

alias n=nvim

##### Prompt {{{1
nix_shell=${NIX_SHELL_PACKAGES:+"{ $NIX_SHELL_PACKAGES } "}
PROMPT="${nix_shell}%{%F{green}%}%v%{%f%} %{%F{blue}%}%(4~|%-1~/…/%2~|%3~)%{%f%} %# "

##### Input & Bindings {{{1

KEYTIMEOUT=10

bindkey -v # Vim editing

# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind up and down keys
zmodload -F zsh/terminfo +p:terminfo
if [[ -n ${terminfo[kcuu1]} && -n ${terminfo[kcud1]} ]]; then
  bindkey ${terminfo[kcuu1]} history-substring-search-up
  bindkey ${terminfo[kcud1]} history-substring-search-down
fi

bindkey '^J' history-substring-search-down
bindkey '^K' history-substring-search-up
bindkey -M vicmd 'k'  history-substring-search-up
bindkey -M vicmd 'j'  history-substring-search-down
bindkey -M vicmd '^J' history-substring-search-down
bindkey -M vicmd '^K' history-substring-search-up
bindkey '^N' expand-or-complete
bindkey '^P' reverse-menu-complete

export VISUAL=nvim
export EDITOR=$VISUAL

##### Change cursor shape {{{1
function zle-keymap-select zle-line-init
{
    # change cursor shape
    if [[ -n "$TMUX" ]]; then  # tmux
      case $KEYMAP in
          vicmd)      print -n '\033[0 q';; # block cursor
          viins|main) print -n '\033[6 q';; # line cursor
      esac
    else # iTerm2
      case $KEYMAP in
          vicmd)      print -n -- "\1\e[2 q\2";;  # block cursor
          viins|main) print -n -- "\1\e[6 q\2";;  # line cursor
      esac
    fi

    zle reset-prompt
    zle -R
}

function zle-line-finish
{
    if [[ -n "$TMUX" ]]; then # tmux
      print -n -- '\033[0 q'  # block cursor
    else # iTerm2
      print -n -- "\1\e[2 q\2"  # block cursor
    fi
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select
