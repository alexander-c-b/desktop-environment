local paren_pairs = { '()', '[]', '{}'}

local function substitute(s)
    for _, parentheses in pairs(paren_pairs) do
        s = s:gsub('"(%b' .. parentheses .. ')"', function (s)
            return '\\left' .. s:sub(1, -2) .. '\\right' .. s:sub(-1, -1)
        end)
    end
    return s
end

return {{
    Math = function(el)
        return pandoc.Math(el.mathtype, substitute(el.text))
    end
}}
